from collections import deque

# Class Definition for each node
class TreeNode:
	left = None; 
	right = None;
	def __init__(self, val):
		self.val = val;
		pass

# each node in the queue is assigned left and right node and this left and right node is appended to queue
queue = deque();
root = [];
count = 0;
lines = tuple(open('input.txt', 'r'));

# to store minimum value at each node
minimumValue = {};

# to store next node to be visited in the minimum path
path = {};
for line in lines:
	lineSplit = line.split("  ");
	if len(lineSplit) == 1: 
		temp = TreeNode(lineSplit[0]);
		root.append(temp);
		queue.append(temp);
	else:
		i=0;
		tempStoreNode = [];
		while i<len(lineSplit):
			tempStoreNode.append(TreeNode(int(lineSplit[i])));
			i=i+1;
			pass
		i=1;
		while i<len(tempStoreNode):
			popNode = queue.popleft();
			left = tempStoreNode[i-1];
			right = tempStoreNode[i];
			popNode.right = right;
			popNode.left = left;
			queue.append(left);
			i = i+1;

		# last node on the line is added to the queue
		queue.append(right);		

# traverseMinimumValue root node to get minimum value
def traverseMinimumValue(root):
	if root==None:
		return 0;

	# check if path is already visited
	if root in minimumValue.keys():
		return minimumValue[root];
	left = traverseMinimumValue(root.left);
	right = traverseMinimumValue(root.right);
	
	if left<right:
		path[root] = root.left;
	else:
		path[root] = root.right;	 
	newVal = min(left,right)+int(root.val);
	minimumValue[root] = newVal;
	return int(newVal);
	pass

# traverse minimum path 	
def traveseMinimumPath(root):
	if root==None:
		return;
	print(root.val);
	traveseMinimumPath(path[root]);
	pass

print("Total value in the minimum path");
print(traverseMinimumValue(root[0]));

print("Minimum path in the tree");
traveseMinimumPath(root[0])




