## Abstract
---
Given a text file, where each line i has j=i no of nodes and also has cost associated with that node. Determine a minimum path from root to any leaf node.

## Implementation
---
Convert text file into tree and apply dfs on it.


### Data Structures used:
---
1) Queue to assign left and right child to the node while getting data from input.txt.  
2) Dictionary to increase the efficiency of the algorithm to linear time complexity.  
3) list of objects to store the reference of the instantiated class.  

### Algorithm
---
1) initialize queue with root node.  
2) goto next line , pop node from queue and assign its left and right child.  
3) append left child to the queue.  
4) append last node on the line to the queue.  
5) repeat step 2 for all the lines in the input.  
6) apply dfs on root node.  
7) use bottom up aprroach and return sum(minimum value of left and right child of the node + node value).  
8) At every node you will get the minimum value down the path.  